import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { Welcome } from '../pages/Welcome';
import { UserIdentification } from '../pages/UserIdentification';
import { Confirmation } from '../pages/Confirmation';
import { PlantSelect } from '../pages/PlantSelect';
import { PlantSave } from '../pages/PlantSave';
import { MyPlants } from '../pages/MyPlants';
import AuthRoutes from './tab.routes';
const stackRoutes = createStackNavigator();

//import StackRoutes from './stack.routes';

const Routes = () => (
    <NavigationContainer>
        <stackRoutes.Navigator
            headerMode="none"
            screenOptions={{
                cardStyle: {
                    backgroundColor: "#fff"
                },
            }}
        >

            <stackRoutes.Screen
                name="Welcome"
                component={Welcome}
            />

            <stackRoutes.Screen
                name="UserIdentification"
                component={UserIdentification}
            />

            <stackRoutes.Screen
                name="Confirmation"
                component={Confirmation}
            />

            <stackRoutes.Screen
                name="PlantSelect"
                component={AuthRoutes}
            />

            <stackRoutes.Screen
                name="PlantSave"
                component={PlantSave}
            />

            <stackRoutes.Screen
                name="MyPlants"
                component={AuthRoutes}
            />

        </stackRoutes.Navigator>
    </NavigationContainer>
)

export default Routes;