import firebase from 'firebase';

const firebaseConfig = {
    apiKey: 'AIzaSyASIFZ5cbpkE9nzEolDe30DCFobe-x9WyY',
    authDomain: 'plantmanager-bd66e.firebaseapp.com',
    databaseURL: 'https://plantmanager-bd66e-default-rtdb.firebaseio.com/',
    projectId: 'plantmanager-bd66e',
    storageBucket: 'plantmanager-bd66e.appspot.com',
};

firebase.initializeApp(firebaseConfig);