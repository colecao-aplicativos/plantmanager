import React from 'react';
import { 
    TouchableOpacity, 
    Text,
    TouchableOpacityProps } from 'react-native'

import styles from './style';

interface ButtonProps extends TouchableOpacityProps {
    title: string;
}

export function Button({ title, ...rest} : ButtonProps) {
    return (
        <TouchableOpacity
            style={styles.container}
            activeOpacity={0.7}
            {...rest}
            >
            <Text style={styles.text}>
                {title}
            </Text>
        </TouchableOpacity>
    )
}