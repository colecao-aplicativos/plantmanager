import axios from 'axios';

const api = axios.create({
    //baseURL: 'https://plantmanager-bd66e-default-rtdb.firebaseio.com/',
    baseURL: 'https://605a60b027f0050017c04dc0.mockapi.io/',
})

export default api;