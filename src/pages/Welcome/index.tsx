import React from 'react';
import {
    SafeAreaView,
    Text,
    Image,
    TouchableOpacity,
    View
} from 'react-native';
import { Feather } from '@expo/vector-icons';
import styles from './style';
import weteringImg from '../../assets/watering.png';
import { useNavigation } from '@react-navigation/core';
import AsyncSorage  from '@react-native-async-storage/async-storage';

export function Welcome() {
    const navigation = useNavigation();
    async function handleStart(){
        const name =  await AsyncSorage.getItem('@plantmanager:user');
        if (name !== null) {
            await AsyncSorage.setItem('@plantmanager:user', name);
            navigation.navigate('MyPlants');
        } else {
            navigation.navigate('UserIdentification');
        }
    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.wrapper}>
                <Text style={styles.title}>
                    Gerencie {'\n'}
                suas plantas de {'\n'}
                forma fácil
            </Text>
                <Image
                    source={weteringImg}
                    style={styles.image}
                    resizeMode="contain"
                />
                <Text style={styles.subtitle}>
                    Não esqueça mais de regar suas plantas.{'\n'}
                Nós cuidamos de lembrar você
                sempre que precisar.
            </Text>
                <TouchableOpacity
                    style={styles.button}
                    activeOpacity={0.7}
                    onPress={handleStart}
                >
                    <Feather
                        name="chevron-right"
                        style={styles.buttonIcon} />
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    )
}