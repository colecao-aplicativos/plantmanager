import React, { useEffect, useState } from 'react';
import {
    Text,
    View,
    FlatList,
} from 'react-native';
import { useNavigation } from '@react-navigation/core';
import { EnviromentButton } from '../../components/EnviromentButton';
import { PlantCardPrimary } from '../../components/PlantCardPrimary';
import { Header } from '../../components/Header';
import { Load } from '../../components/Load';
import { PlantProps } from '../../libs/storage';
import api from '../../services/api';
import styles from './style';

export function PlantSelect() {
    interface EnviromentsProps {
        key: string;
        title: string;
    }

    const navigation = useNavigation();
    const [enviroments, setEnviroments] = useState<EnviromentsProps[]>();
    const [plants, setPlants] = useState<PlantProps[]>();
    const [filteredplants, setFilteredplants] = useState<PlantProps[]>();
    const [enviromentSelected, setEnviromentSelected] = useState('all');
    const [loading, setLoading] = useState(true);

    function handleEnviromentSelected(enviroment: string){
        setEnviromentSelected(enviroment);
        if(enviroment ==='all')
            return setFilteredplants(plants);
        const filtered = plants?.filter(plant =>
                plant.environments.includes(enviroment)
        );
        setFilteredplants(filtered);
    }

    function handlePlantSelect(plant: PlantProps){
        navigation.navigate('PlantSave', { plant });
    }

    useEffect(() => {
        async function fetchEnviroment() {
            const { data } = await api
                .get('plants_environments');
            setEnviroments([
                {
                    key: 'all',
                    title: 'Todos',
                },
                ...data
            ]);
        }

        fetchEnviroment();

    }, [])

    useEffect(() => {
        async function fetchPlants() {
            const { data } = await api.get('plants');
            setPlants(data);
            setFilteredplants(data);
            setLoading(false);
        }
        fetchPlants();
    }, [])

    if(loading)
        return <Load />

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Header />
                <Text style={styles.title}>
                    Em qual ambiente
                </Text>
                <Text style={styles.subtitle}>
                    você quer colocar sua planta?
                </Text>
            </View>
            <View>
                <FlatList
                    keyExtractor={(item) => String(item.key)}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    data={enviroments}
                    contentContainerStyle={styles.enviromentList}
                    renderItem={({ item }) => (
                        <EnviromentButton
                            title={item.title}
                            active={item.key === enviromentSelected}
                            onPress={() => handleEnviromentSelected(item.key)}
                        />
                    )} />
            </View>
            <View style={styles.plants}> 
                <FlatList  
                    keyExtractor={(item) => String(item.id)}
                    contentContainerStyle={styles.contentContainerStyle}
                    showsVerticalScrollIndicator={false}
                    numColumns={2}
                    data={filteredplants}
                    renderItem={({ item }) => (
                        <PlantCardPrimary
                            data={item} 
                            onPress={() => handlePlantSelect(item)}/>
                    )}
                />
            </View>
        </View>
    );
}