import React, { useEffect, useState } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    Alert,
} from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { pt } from 'date-fns/locale';
import { formatDistance } from 'date-fns';
import {
    loadPlant,
    PlantProps,
    removePlant
} from '../../libs/storage';
import { Header } from '../../components/Header';
import waterdrop from '../../assets/waterdrop.png'
import styles from './style';
import { PlantCardSecundary } from '../../components/PlantCardSecundary';
import { Load } from '../../components/Load';

export function MyPlants() {
    const [myPlants, setMyPlants] = useState<PlantProps[]>([]);
    const [loading, setLoading] = useState(true);
    const [nextWaterd, setNextWaterd] = useState<string>();

    function handleRemove(plant: PlantProps) {
        Alert.alert('Remover', `Deseja remover a ${plant.name}?`, [
            {
                text: 'Não 🙏',
                style: 'cancel'
            },
            {
                text: 'Sim 😥',
                onPress: async () => {
                    try {
                        await removePlant(plant.id)
                        setMyPlants((oldData) =>
                            oldData.filter((item) => item.id != plant.id)
                        );
                    } catch (error) {
                        Alert.alert('Não foi possivel remover');
                    }
                }
            }
        ])
    }

    useEffect(() => {
        async function loadStorageData() {
            const plantsStoraged = await loadPlant();

            if (plantsStoraged.length > 0) {
                const nextTime = formatDistance(
                    new Date(plantsStoraged[0]
                        .dateTimeNotification).getTime(),
                    new Date().getTime(),
                    { locale: pt },
                );
                setNextWaterd(
                    `Regue sua ${plantsStoraged[0].name} daqui à ${nextTime}.`
                );
            } else {
                setNextWaterd(
                    `Voce nao selecionou nenhuma planta!`
                );
            }
            setMyPlants(plantsStoraged);
            setLoading(false);
        };
        loadStorageData();
    }, []);

    if (loading)
        return <Load />

    return (
        <View style={styles.container}>
            <Header />

            <View style={styles.spotlight}>
                <Image
                    style={styles.spotlightImage}
                    source={waterdrop} />

                <Text style={styles.spotlightText}>
                    {nextWaterd}
                </Text>
            </View>
            <View style={styles.plants}>
                <Text style={styles.plantsTitle}>
                    Proximas Regadas
                </Text>

                <ScrollView
                    showsVerticalScrollIndicator={false}
                    horizontal={false}>
                    <FlatList
                        data={myPlants}
                        keyExtractor={(item) => String(item.id)}
                        renderItem={({ item }) => (
                            <PlantCardSecundary
                                data={item}
                                handleRemove={() => { handleRemove(item) }}
                            />
                        )}
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{ flex: 1 }}
                    />
                </ScrollView>
            </View>
        </View>

    );
}